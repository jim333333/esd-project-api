const server = require('./src/server')
const config = require('./src/config')
const mongoose = require('mongoose')
const async = require('async')
const logger = require('./src/utils/logger')
const port = process.env.PORT || config.port;

mongoose.Promise = global.Promise

function initializeMongo (callback) { // TODO: use connection url from config depending on the environment
  mongoose.connect('mongodb://admin:root@ds113936.mlab.com:13936/esd-project', {
    useMongoClient: true
  }, callback)
}

function initializeServer (callback) {
  server.listen(port, callback)
}

async.parallel([
  initializeMongo,
  initializeServer
], (error) => {
  if (error) {
    return logger.error(error)
  }

  logger.info(`Connected to MongoDB at ${config.mongoUri}`)
  logger.info(`Server is running at ${config.url}${config.port}`)
})
