const mongoose = require('mongoose')

const dataSchema = mongoose.Schema({
  tmp: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    required: true
  }
})

const Data = mongoose.model('Data', dataSchema)

module.exports = Data
