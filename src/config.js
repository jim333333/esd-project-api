const pkg = require('../package.json')

const PORT = process.env.PORT || 8080;

module.exports = {
  name: pkg.name,
  version: pkg.version,
  port: PORT,
  mongoUri: `mongodb://127.0.0.1:27017/${pkg.name}`,
  url: 'http://localhost:'
}
