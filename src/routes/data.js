const router = module.exports = require('express').Router()
const Data = require('../modals/data')
const moment = require('moment')

function startGeneratingTmpData() {
  const intervalId = setInterval(() => {
    const tmp = Math.random() * (30 - 10) + 10
    const date = moment()

    Data.create({ tmp, date }, (error) => {
      if (error) {
        clearInterval(intervalId)
        console.log('error generating tmp data..')
      }

      console.log('generated tmp data: ', tmp, moment(date))
    })
  }, 5000)
}

// startGeneratingTmpData()

router.get('/data', (req, res, next) => {
  Data.find().sort({ $natural: -1 }).limit(20).exec((error, data) => {
    if (error) {
      return next(error)
    }

    res.send(data)
  })
})

router.post('/data', (req, res, next) => {
  const { body: { tmp } } = req
  const date = moment()

  Data.create({ tmp, date }, (error) => {
    if (error) {
      return next(error)
    }

    res.sendStatus(205)
  })
})

